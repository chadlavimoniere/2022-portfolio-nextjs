export interface Video {
  title: string;
  url: string;
  embed: string;
  description?: string;
  date: string;
}

export const videos: Video[] = [
  {
    title: "Setting up Duo Workflow as a UX team member",
    url: "https://www.youtube.com/watch?v=tVcScOmx_m4",
    embed: "https://www.youtube.com/embed/tVcScOmx_m4?si=7_i3T7rO-TWxhB_9",
    date: "2025-01-28",
  },
  {
    title: "Proposal: board list page",
    url: "https://www.youtube.com/watch?v=P_1o-RYxxbs",
    embed: "https://www.youtube.com/embed/P_1o-RYxxbs?si=DMax3IwJH5xUVhWJ",
    date: "2024-11-01",
  },
  {
    title: "Using the + delimiter to add Duo context in message",
    url: "https://www.youtube.com/watch?v=zpqZbufjRrM",
    embed: "https://www.youtube.com/embed/zpqZbufjRrM?si=x0oU56yFlnFss_C7",
    date: "2024-10-31",
  },
  {
    title:
      "Design: Using GitLab conventions to include context in Duo chat messages",
    url: "https://www.youtube.com/watch?v=UD0oKcP6KwQ",
    embed: "https://www.youtube.com/embed/UD0oKcP6KwQ?si=roegfh_vrhdunxZC",
    date: "2024-10-28",
  },
  {
    title:
      "UX Showcase - UNO Reverse: teach me about how traditional designers work at GitLab",
    url: "https://www.youtube.com/watch?v=KfdeVFMdp7I",
    embed:
      "https://www.youtube-nocookie.com/embed/KfdeVFMdp7I?si=40rXLmvmJcf08uRq",
    date: "2024-03-20",
  },
  {
    title: "UX Forum - GitLab Status Minder",
    url: "https://www.youtube.com/watch?v=jf3RKeO8Iq0",
    embed: "https://www.youtube.com/embed/jf3RKeO8Iq0?si=aRjTTx8UJP98NzIP",
    date: "2024-08-21",
  },
];
