export const dateOptions: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "long",
  day: "numeric",
};

export const PROJECT_URL =
  "https://gitlab.com/chadlavimoniere/2022-portfolio-nextjs/-/blob/main";

export const DEFAULT_SEO_IMAGE_URL =
  "https://www.chadlavimoniere.com/chad-about-me.jpeg";
export const DEFAULT_SEO_DESCRIPTION =
  "Chad Lavimoniere is a Product Designer currently working at GitLab";

export const SPRING_2024_POTTERY_IMAGES = [...Array(98)].map(
  (_, i) =>
    `/pottery/2024/spring/2024-spring-pottery-${
      i < 9 ? `0${i + 1}` : i + 1
    }.jpeg`
);

export const SUMMER_2024_POTTERY_IMAGES = [...Array(84)].map(
  (_, i) =>
    `/pottery/2024/summer/2024-summer-pottery-${
      i < 9 ? `0${i + 1}` : i + 1
    }.jpeg`
);

export const FALL_2024_POTTERY_IMAGES = [...Array(113)].map(
  (_, i) =>
    `/pottery/2024/fall/2024-fall-pottery-${
      i < 9 ? `00${i + 1}` : i < 99 ? `0${i + 1}` : i + 1
    }.jpeg`
);
