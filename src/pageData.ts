import { DEFAULT_SEO_DESCRIPTION, DEFAULT_SEO_IMAGE_URL } from "./constants";

export interface PageProps {
  /** Page title */
  title: string;
  /** Page href within the projects dir */
  href: string;
  /** Preview snippet for project pages. Default is DEFAULT_SEO_DESCRIPTION */
  snippet?: string;
  /** Page is a project page */
  project?: boolean;
  /** Page is a blog page */
  blog?: boolean;
  type?: "blog" | "project";
  hidden?: boolean;
  date?: string;
  /** Use a specific image for unfurl previews. default is DEFAULT_SEO_IMAGE_URL */
  seoImageUrl?: string;
  /** Use a specific title for unfurl previews. default is undefined. */
  seoTitle?: string;
}

/**
 * Anything added to this array will have `blog: true` appended to it.
 *
 * Blogs will be sorted in the exact order shown here.
 */
const blogs: PageProps[] = [
  {
    title: "My workout routine",
    href: "six-week-shred",
    snippet: "These are the workouts I'm currently doing",
    date: "2024-03-06",
  },
];

const sharedProps: Partial<PageProps> = {
  seoImageUrl: DEFAULT_SEO_IMAGE_URL,
  snippet: DEFAULT_SEO_DESCRIPTION,
};

const sharedBlogProps: Partial<PageProps> = {
  ...sharedProps,
  type: "blog",
  blog: true,
};

/**
 * Anything added to this array will have `project: true` appended to it.
 *
 * Projects will be sorted in the exact order shown here.
 */
const projects: PageProps[] = [
  {
    title: "2024 Fall semester pottery",
    href: "2024-fall-pottery",
    snippet:
      "The pots I created in the fall 2024 wheel throwing class at Clay Space Brooklyn",
    date: "2024-11-14",
    seoImageUrl:
      "https://www.chadlavimoniere.com/pottery/2024/fall/2024-fall-pottery-065.jpeg",
  },
  {
    title: "Fantasy town generator",
    href: "fantasy-town-generator",
    snippet: "A simple tool for generating fantasy towns for tabletop gaming.",
    date: "2024-09-13",
  },
  {
    title: "2024 Summer semester pottery",
    href: "2024-summer-pottery",
    snippet:
      "The pots I created in the summer 2024 wheel throwing class at Clay Space Brooklyn",
    date: "2024-09-10",
    seoImageUrl:
      "https://www.chadlavimoniere.com/pottery/2024/summer/2024-summer-pottery-37.jpeg",
  },
  {
    title: "Clay shrinkage calculator",
    href: "clay-shrinkage-calculator",
    snippet: "A simple tool for calculating the shrinkage of clay when fired",
    date: "2024-07-07",
  },
  {
    title: "2024 Spring semester pottery",
    href: "2024-spring-pottery",
    snippet:
      "The pots I created in the spring 2024 wheel throwing class at Clay Space Brooklyn",
    date: "2024-07-01",
    seoImageUrl:
      "https://www.chadlavimoniere.com/pottery/2024/spring/2024-spring-pottery-28.jpeg",
  },
  {
    title: "GitLab Status Minder",
    href: "gitlab-status-minder",
    snippet: "A small rails app for scheduling future GitLab status messages",
    date: "2024-06-24",
  },
  {
    title: "Lasers & Feelings character sheet",
    href: "lasers-feelings",
    snippet: "a simple Lasers & Feelings character sheet in the browser",
    date: "2024-02-17",
  },
  {
    title: "react-highlighter-ts",
    href: "react-highlighter",
    snippet: "a TypeScript react library for creating dynamic text highlights",
  },
  {
    title: "Recipes",
    href: "recipes",
    snippet:
      "I keep all my recipes in a git repo of markdown files. Frequently updated!",
  },
  {
    title: "Alone among the stars",
    href: "alone-among-the-stars",
    snippet: "A tiny web version of a cool card game",
  },
  {
    title: "Bug prioritizer",
    href: "bug-prioritizer",
    snippet:
      "A little bug severity calculator I knocked together when we were figuring out a bug triage system at Casebook in 2020",
  },
  {
    title: "Coffee pacer",
    href: "coffee-pacer",
    snippet: "A timer I made for making pour-over coffee",
  },
  {
    title: "Choose your own adventure",
    href: "cyoa",
    snippet:
      "An (unfinished) experiment with making choose your own adventure stories",
  },
  {
    title: "Emoji HTML code lookup",
    href: "emoji-reference",
    snippet: "A simple lookup for Emoji HTML entities",
  },
  {
    title: "Hexle",
    href: "hexle",
    snippet: "It's like wordle, but you guess the hex code for a random color",
  },
  {
    title: "What to listen to?",
    href: "lavi-vinyl",
    snippet:
      "A little glitch app my wife and I made for randomly selecting one of our vinyl records",
  },
  {
    title: "Pour over schedule calculator",
    href: "pour-over-schedule-calculator",
    snippet: "A little calculator I made for timing pour-over coffee",
  },
  {
    title: "Sourdough converter",
    href: "sourdough-converter",
    snippet: "A simple tool to convert normal yeasted recipes to sourdough",
  },
  {
    title: "TNG Sounds",
    href: "tng-sounds",
    snippet: "A little Star Trek The Next Generation soundboard",
  },
  {
    title: "Tuning fork robot",
    href: "tuning-fork",
    snippet:
      "A simple tuning fork web app I made to experiment with the Web Audio API",
  },
  {
    title: "Turnip calc",
    href: "turnip-calc",
    snippet: "A turnip returns calculator for Animal Crossings: New Horizons",
  },
];

const sharedProjectProps: Partial<PageProps> = {
  ...sharedProps,
  type: "project",
  project: true,
};

const pages: PageProps[] = [
  {
    ...sharedProps,
    title: "Experience",
    href: "experience",
    seoImageUrl: "https://www.chadlavimoniere.com/headshot.jpg",
    seoTitle: "Chad Lavimoniere",
  },
  {
    ...sharedProps,
    title: "Personal projects",
    href: "projects",
    snippet: "Some small web projects I've done in my personal time.",
  },
  ...projects.map((p) => ({ ...sharedProjectProps, ...p })),
  {
    ...sharedProps,
    title: "Blog",
    href: "blog",
  },
  ...blogs.map((b) => ({ ...sharedBlogProps, ...b })),
  {
    ...sharedProps,
    title: "Video",
    href: "video",
  },
];

export const getPage = (href: PageProps["href"]) =>
  pages.filter((p) => p.href === href)[0];

/**
 * Defines the links that show in the nav
 */
export const navLinks = [
  {
    title: "Home",
    href: "/",
  },
  {
    title: getPage("experience").title,
    href: "/experience",
  },
  {
    title: getPage("projects").title,
    href: "/projects",
  },
  // {
  //   title: getPage("blog").title,
  //   href: "/blog",
  // },
  {
    title: getPage("video").title,
    href: "/video",
  },
];

export default pages;
