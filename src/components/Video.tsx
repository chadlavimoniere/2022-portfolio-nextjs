import { Video as VideoProps } from "@/videoData";

const Video = ({ embed, description }: VideoProps) => (
  <div className="mb-4">
    <iframe
      className="w-full aspect-video"
      src={embed}
      title="YouTube video player"
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      referrerPolicy="strict-origin-when-cross-origin"
      allowFullScreen
    ></iframe>
    {description && <p className="snippet">{description}</p>}
  </div>
);

export default Video;
