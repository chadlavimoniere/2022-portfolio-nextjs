import { videos, Video as VideoProps } from "@/videoData";
import Video from "./Video";

const sortByDate = (a: VideoProps, b: VideoProps): number => {
  const dateA = new Date(a.date);
  const dateB = new Date(b.date);
  return dateB.getTime() - dateA.getTime();
};

const Videos = () =>
  videos.sort(sortByDate).map((v) => <Video key={v.url} {...v} />);
export default Videos;
