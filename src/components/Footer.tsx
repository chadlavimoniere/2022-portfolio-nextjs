import { Link } from "@/components";
import { PROJECT_URL } from "@/constants";
import classNames from "classnames";

const Footer = ({ page, className }: { page?: string; className?: string }) => (
  <footer className={classNames("footer", className)}>
    {new Date().getFullYear()} &copy; Chad Lavimoniere{" "}
    {page && (
      <span className="print:hidden">
        (<Link href={`${PROJECT_URL}/src/pages/${page}`}>source</Link>)
      </span>
    )}
  </footer>
);

export default Footer;
