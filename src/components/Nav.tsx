import { PageProps, navLinks } from "@/pageData";
import { useState } from "react";
import IconMenu from "./IconMenu";
import IconClose from "./IconClose";
import ClickAwayListener from "react-click-away-listener";

interface LinkProps extends Pick<PageProps, "href" | "title"> {
  page?: string;
}

const Link = ({ page, href, title }: LinkProps) =>
  page === title ? (
    <span className="opacity-75">{title}</span>
  ) : (
    <a href={href} className="link">
      {title}
    </a>
  );

export default function Nav({ page }: { page?: LinkProps["page"] }) {
  const [showMenu, setShowMenu] = useState(false);
  const toggleMenu = () => setShowMenu((s) => !s);
  const closeMenu = () => setShowMenu(false);
  return (
    <>
      <nav className="justify-end hidden sm:flex gap-3 my-4 print:hidden">
        {navLinks.map((link) => (
          <Link page={page} key={link.title} {...link} />
        ))}
      </nav>
      <ClickAwayListener onClickAway={closeMenu}>
        <div className="fixed h-0 top-4 right-4 sm:hidden text-right z-10">
          <button
            title={showMenu ? "Close menu" : "Open menu"}
            className="border p-2 inline-flex items-center background"
            onClick={toggleMenu}
          >
            {showMenu ? <IconClose /> : <IconMenu />}
          </button>
          {showMenu && (
            <nav className="border p-3 justify-end flex flex-wrap gap-3 mt-[-1px] ml-4 background">
              {navLinks.map((link) => (
                <Link page={page} key={link.title} {...link} />
              ))}
            </nav>
          )}
        </div>
      </ClickAwayListener>
    </>
  );
}
