import { ComponentPropsWithRef } from "react";
import { Link, Paragraph } from "@/components";
import classNames from "classnames";

export function useMDXComponents(components) {
  return {
    ...components,
    h1: (props: ComponentPropsWithRef<"h1">) => (
      <h1 {...props} className={classNames("h1", props.className)} />
    ),
    h2: (props: ComponentPropsWithRef<"h2">) => (
      <h2 {...props} className={classNames("h2", props.className)} />
    ),
    h3: (props: ComponentPropsWithRef<"h3">) => (
      <h3 {...props} className={classNames("h3", props.className)} />
    ),
    a: Link,
    p: Paragraph,
    hr: (props) => <hr {...props} className="hr" />,
    ul: (props: ComponentPropsWithRef<"ul">) => (
      <ul {...props} className="ul list" />
    ),
    ol: (props: ComponentPropsWithRef<"ol">) => (
      <ol {...props} className="ol list" />
    ),
    li: (props: ComponentPropsWithRef<"li">) => (
      <li {...props} className={classNames("li", props.className)} />
    ),
    img: (props: ComponentPropsWithRef<"img">) => (
      <img {...props} className={classNames("img", props.className)} />
    ),
    code: (props: ComponentPropsWithRef<"code">) => (
      <code {...props} className={classNames("code", props.className)} />
    ),
  };
}
