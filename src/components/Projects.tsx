import pages from "@/pageData"
import Listing from "./Listing";

const Projects = () => pages
  .filter(p => (p.project && !p.hidden))
  .map(p => <Listing directory="projects" key={p.href} {...p} />)

export default Projects;
