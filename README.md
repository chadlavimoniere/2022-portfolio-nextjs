# README.md

My portfolio site, rebuilt with next.js

The **main** branch gets published. Keep drafts on the **draft** branch!

See also [Dev notes](./DEV_NOTES.md)
