import classNames from "classnames";
import { ComponentPropsWithRef } from "react";

const Link = (props: ComponentPropsWithRef<"a">) => (
  <a
    {...props}
    target={props?.href?.match(/^http/) ? "_blank" : undefined}
    className={classNames("link", props.className)}
  />
);

export default Link;
