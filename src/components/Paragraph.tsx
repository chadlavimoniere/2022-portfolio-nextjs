import classNames from "classnames";
import { ComponentPropsWithRef } from "react";

const Paragraph = (props: ComponentPropsWithRef<"p">) => (
  <p {...props} className={classNames("p", props.className)} />
);

export default Paragraph;
