export interface ImageGalleryProps {
  urls: string[];
  openInNewTab?: boolean;
}

const ImageGallery = ({ urls, openInNewTab = true }: ImageGalleryProps) => (
  <div className="flex flex-wrap gap-4">
    {urls.map((url, i) => (
      <a
        key={i}
        href={url}
        target={openInNewTab ? "_blank" : undefined}
        className="w-1/6 max-sm:w-2/5 grow flex"
      >
        <img
          src={url}
          alt={`Pot ${i + 1}`}
          className="!m-0 object-cover aspect-auto grow"
          loading="lazy"
        />
      </a>
    ))}
  </div>
);

export default ImageGallery;
