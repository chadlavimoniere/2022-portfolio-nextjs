import { PageProps } from "@/pageData";
import Head from "next/head";

const HeadComponent = ({
  project: { title, snippet, seoImageUrl, seoTitle },
}: {
  project: Partial<PageProps>;
}) => (
  <Head>
    {title && <title>{`${title} — Chad Lavimoniere`}</title>}
    {seoTitle && <meta property="og:title" content={seoTitle} />}
    {snippet && (
      <>
        <meta name="description" content={snippet} />
        <meta property="og:description" content={snippet} />
      </>
    )}
    {seoImageUrl && <meta property="og:image" content={seoImageUrl} />}
  </Head>
);

export default HeadComponent;
