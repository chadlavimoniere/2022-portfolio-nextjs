import { dateOptions } from "@/constants";
import { PageProps } from "@/pageData";

interface ListingProps extends Partial<PageProps> {
  directory?: string
}

const Listing = ({ title, href, snippet, date, directory = "projects" }: ListingProps) => {
  let dateString = "";
  if (date) {
    dateString = new Date(`${date}T00:00:00`).toLocaleDateString(undefined, dateOptions)
  }
  return (<>
    <h2 className="h2">
      <a href={`${directory}/${href}`} className="link !no-underline">{title}</a>
    </h2>
    {date && <div className="date">{dateString}</div>}
    <p className="snippet">{snippet}</p>
  </>)
};

export default Listing