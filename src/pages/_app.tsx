import "@/styles/globals.css";
import { useEffect } from "react";

export default function App({ Component, pageProps }) {
  useEffect(() => {
    window.addEventListener("beforeprint", (event) => {
      const details = Array.from(document.querySelectorAll("details"));
      for (const detailEl of details) {
        if (detailEl.getAttribute("open") == null) {
          detailEl.setAttribute("data-was-closed", "true");
        }
        detailEl.setAttribute("open", "");
      }
    });

    window.addEventListener("afterprint", (event) => {
      const details = Array.from(document.querySelectorAll("details"));
      for (const detailEl of details) {
        if (detailEl.getAttribute("data-was-closed") != null) {
          detailEl.removeAttribute("data-was-closed");
          detailEl.removeAttribute("open");
        }
      }
    });
  }, []);
  return <Component {...pageProps} />;
}
